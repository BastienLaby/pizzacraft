#ifndef _IMAC2GL3_SHADER_TOOLS_HPP_
#define _IMAC2GL3_SHADER_TOOLS_HPP_

#include <GL/glew.h>

namespace imac2gl3 {

	GLuint loadProgram(const char* vertexShaderFile, const char* fragmentShaderFile);
	const GLvoid* BufferOffset(size_t offset);

}

#endif
