#ifndef _IMAC2GL3_GLSHAPEINSTANCE_HPP_
#define _IMAC2GL3_GLSHAPEINSTANCE_HPP_

#define POSITION_LOCATION 0
#define NORMAL_LOCATION 1
#define TEXCOORDS_LOCATION 2

#include <GL/glew.h>
#include <imac2gl3/shapes/Sphere.hpp>
#include <imac2gl3/shapes/Cube.hpp>
#include <imac2gl3/shapes/Cone.hpp>
#include <imac2gl3/shapes/Cylinder.hpp>
#include <iostream>

class GLShapeInstance{

	GLuint m_VBO;
	GLuint m_VAO;
	size_t m_VertexCount;
	GLShapeInstance(const GLShapeInstance & source){}   
	GLShapeInstance & operator =(const GLShapeInstance & glShapeInstance);
	
	public :
		GLShapeInstance (const imac2gl3::Sphere & sphere);
		GLShapeInstance (const imac2gl3::Cube & cube);
		GLShapeInstance (const imac2gl3::Cone & cube);
		GLShapeInstance (const imac2gl3::Cylinder & cube);
		~GLShapeInstance();
		void draw() const;
};

#endif
