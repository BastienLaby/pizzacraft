#include <iostream>
#include <fstream>
#include <string>
#include <GL/glew.h>

namespace imac2gl3 {

	static const char* readFile(const char* filePath) {
	    std::ifstream file(filePath);

	    if (!file) {
	        return 0;
	    }

	    file.seekg(0, std::ios_base::end);
	    size_t length = file.tellg();
	    file.seekg(0);

	    char* src = new char[length + 1];
	    file.read(src, length);
	    src[length] = '\0';
	    
	    return src;
	}

	GLuint loadProgram(const char* vertexShaderFile, const char* fragmentShaderFile) {
	    
	    const char* vertexShaderSource = readFile(vertexShaderFile);
	    if(!vertexShaderSource) {
	        std::cerr << "Unable to load " << vertexShaderFile << std::endl;
	        return 0;
	    }
	    
	    const char* fragmentShaderSource = readFile(fragmentShaderFile);
	    if(!fragmentShaderSource) {
	        std::cerr << "Unable to load " << fragmentShaderFile << std::endl;
	        return 0;
	    }
	    
	    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	    glShaderSource(vertexShader, 1, &vertexShaderSource, 0);
	    glCompileShader(vertexShader);
	    
	    GLint compileStatus;
	    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compileStatus);
	    if(compileStatus == GL_FALSE) {
	        GLint logLength;
	        glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &logLength);
	        char* log = new char[logLength];
	        glGetShaderInfoLog(vertexShader, logLength, 0, log);
	        std::cerr << "Vertex Shader error:" << log << std::endl;
	        std::cerr << vertexShaderSource << std::endl;
	        delete [] log;
	        return 0;
	    }
	    
	    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);	    
	    glShaderSource(fragmentShader, 1, &fragmentShaderSource, 0);	    
	    glCompileShader(fragmentShader);

	    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compileStatus);
	    if(compileStatus == GL_FALSE) {
	        GLint logLength;
	        glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &logLength);
	        char* log = new char[logLength];
	        glGetShaderInfoLog(fragmentShader, logLength, 0, log);
	        std::cerr << "Fragment Shader error:" << log << std::endl;
	        std::cerr << fragmentShaderSource << std::endl;
	        delete [] log;
	        return 0;
	    }
	    
	    GLuint program = glCreateProgram();	    
	    glAttachShader(program, vertexShader);
	    glAttachShader(program, fragmentShader);	    
	    glDeleteShader(vertexShader);
	    glDeleteShader(fragmentShader);	    
	    glLinkProgram(program);

	    GLint linkStatus;
	    glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	    if(linkStatus == GL_FALSE) {
	        GLint logLength;
	        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);	        
	        char* log = new char[logLength];	        
	        glGetProgramInfoLog(program, logLength, 0, log);
	        std::cerr << "Program link error:" << log << std::endl;	        
	        delete [] log;
	        return 0;
	    }
	    
	    delete [] vertexShaderSource;
	    delete [] fragmentShaderSource;
	    return program;
	}
}