#ifndef _IMAC2GL3_TEXTURE_TOOLS_
#define _IMAC2GL3_TEXTURE_TOOLS_

#include <GL/glew.h>
#include <SDL/SDL.h>

namespace imac2gl3 {

	GLenum getTextureFormat(const SDL_Surface* pTexture);
	
}

#endif