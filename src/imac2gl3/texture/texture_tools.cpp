#include <imac2gl3/texture/textur_tools.hpp>

namespace imac2gl3 {

	GLenum getTextureFormat(const SDL_Surface* pTexture) {
		switch(pTexture->format->BytesPerPixel) {
			case 1 :
				return GL_RED; 
			break;
			case 3 :
				return GL_RGB;
			break;
			case 4 :
				return GL_RGBA;
			break;
			default :
				return GL_RGB;
			break;
		}
	}
	
}