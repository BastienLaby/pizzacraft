#include <cmath>
#include <vector>
#include <iostream>
#include "imac2gl3/shapes/common.hpp"
#include "imac2gl3/shapes/Cube.hpp"

namespace imac2gl3 {

	void Cube::build(GLfloat size) {
	    
	    // On créé le tableau de vertex (36 vertex * 8 composantes par vertex = 36*8 GLfloat dans le tableau
	    // rappel : l'axe des Z pointe vers nous
	    GLfloat vertices[36*TOTAL_NUM_COMPONENTS] = {
	    
	    		// face en bas
	    		-0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // posx, posy, posz, normalx, normaly, normalz, texcoordx, texcoordy
	    		-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
	    		0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
	    		
	    		-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
	    		0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
	    		0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	    		
	    		//face en haut
	    		-0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
	    		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
	    		0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
	    		
	    		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
	    		0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
	    		0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
	    		
	    		//face à gauche
	    		-0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
	    		-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	    		-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	    		
	    		-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	    		-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	    		-0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	    		
	    		// face à droite
	    		0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	    		0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	    		0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
	    		
	    		0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	    		0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
	    		0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	    		
	    		//face devant
	    		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	    		0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	    		-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
	    		
	    		0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	    		-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
	    		0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	    		
	    		//face derrière
	    		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
	    		0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
	    		-0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
	    		
	    		0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
	    		-0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
	    		0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f   		
	    };
	    
	    // On remplit chaque vertex du dataPointer avec les valeurs du tableau
	    // -> A chaque ShapeVertex du tableau de vertex (m_pDataPointer) correspond une ligne (8 GLfloat) du tableau vertices
	    for(GLsizei i = 0; i<36*TOTAL_NUM_COMPONENTS; i = i + TOTAL_NUM_COMPONENTS){
	    
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].position.x = vertices[i]*size;
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].position.y = vertices[i+1]*size;
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].position.z = vertices[i+2]*size;
		
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].normal.x = vertices[i+3];
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].normal.y = vertices[i+4];
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].normal.z = vertices[i+5];
		
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].texCoords.x = vertices[i+6];
			m_pDataPointer[i/TOTAL_NUM_COMPONENTS].texCoords.y = vertices[i+7];
	    }
	}
}

