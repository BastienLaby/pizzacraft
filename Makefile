COMPIL = g++

COMPILFLAGS = -Iinclude
LINKFLAGS = -lSDL -lSDL_image

SRC_PATH = src
INC_PATH = include
OBJ_PATH = obj
BIN_PATH = bin

OBJ_FILES = $(OBJ_PATH)/main.o $(OBJ_PATH)/function.o

BIN_NAME = mapGenerator
EXE = $(BIN_PATH)/$(BIN_NAME)

#main
$(EXE) : $(OBJ_FILES)
	@ #linking main.o
	@ $(COMPIL) $(COMPILFLAGS) $(LINKFLAGS) $(OBJ_FILES) -o $@
	@echo "***************************************"
	@echo "         LINKING & COMPILING           "
	@echo "***************************************"
	@echo " > $(EXE) IS READY TO BE USED"

$(OBJ_PATH)/main.o : $(SRC_PATH)/main.cpp
	@ #compiling main.o
	@ $(COMPIL) $(COMPILFLAGS) -c $< -o $@


#other obj
$(OBJ_PATH)/function.o : $(SRC_PATH)/function.cpp $(INC_PATH)/function.hpp
	@ #compiling function.o
	@ $(COMPIL) $(COMPILFLAGS) -c $< -o $@

clean :
	@echo "***************************************"
	@echo "             CLEANING                  "
	@echo "***************************************"
	@$(RM) $(OBJ_PATH)/*.o
	@$(RM) $(BIN_PATH)/*.bin
	@echo " > CLEANING OK (bin & obj have been removed). "

