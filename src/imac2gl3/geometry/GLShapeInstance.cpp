#include <imac2gl3/shapes/GLShapeInstance.hpp>

GLShapeInstance::GLShapeInstance (const imac2gl3::Sphere & sphere){
	
			glGenBuffers(1, &m_VBO);
			m_VertexCount = sphere.getVertexCount();
			
			glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
				glBufferData(GL_ARRAY_BUFFER, sphere.getByteSize(), sphere.getDataPointer(), GL_STATIC_DRAW); 
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			
			glGenVertexArrays(1, &m_VAO);
			glBindVertexArray(m_VAO);
				glEnableVertexAttribArray(POSITION_LOCATION);
				glEnableVertexAttribArray(NORMAL_LOCATION);
				glEnableVertexAttribArray(TEXCOORDS_LOCATION);
				glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
					glVertexAttribPointer(	POSITION_LOCATION,
											sphere.getPositionNumComponents(),
											sphere.getDataType(),
											GL_FALSE,
											sphere.getVertexByteSize(),
											sphere.getPositionOffset()
					);
					glVertexAttribPointer(	NORMAL_LOCATION,
											sphere.getNormalNumComponents(),
											sphere.getDataType(),
											GL_FALSE,
											sphere.getVertexByteSize(),
											sphere.getNormalOffset()
					);
					glVertexAttribPointer(	TEXCOORDS_LOCATION,
											sphere.getTexCoordsNumComponents(),
											sphere.getDataType(),
											GL_FALSE,
											sphere.getVertexByteSize(),
											sphere.getTexCoordsOffset()
					);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
}

GLShapeInstance::GLShapeInstance (const imac2gl3::Cube & cube){
	
			glGenBuffers(1, &m_VBO);
			m_VertexCount = cube.getVertexCount();
			
			glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
				glBufferData(GL_ARRAY_BUFFER, cube.getByteSize(), cube.getDataPointer(), GL_STATIC_DRAW); 
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			
			glGenVertexArrays(1, &m_VAO);
			glBindVertexArray(m_VAO);
				glEnableVertexAttribArray(POSITION_LOCATION);
				glEnableVertexAttribArray(NORMAL_LOCATION);
				glEnableVertexAttribArray(TEXCOORDS_LOCATION);
				glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
					glVertexAttribPointer(	POSITION_LOCATION,
											cube.getPositionNumComponents(),
											cube.getDataType(),
											GL_FALSE,
											cube.getVertexByteSize(),
											cube.getPositionOffset()
					);
					glVertexAttribPointer(	NORMAL_LOCATION,
											cube.getNormalNumComponents(),
											cube.getDataType(),
											GL_FALSE,
											cube.getVertexByteSize(),
											cube.getNormalOffset()
					);
					glVertexAttribPointer(	TEXCOORDS_LOCATION,
											cube.getTexCoordsNumComponents(),
											cube.getDataType(),
											GL_FALSE,
											cube.getVertexByteSize(),
											cube.getTexCoordsOffset()
					);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
}

GLShapeInstance::GLShapeInstance (const imac2gl3::Cone & cone){
	
			glGenBuffers(1, &m_VBO);
			m_VertexCount = cone.getVertexCount();
			
			glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
				glBufferData(GL_ARRAY_BUFFER, cone.getByteSize(), cone.getDataPointer(), GL_STATIC_DRAW); 
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			
			glGenVertexArrays(1, &m_VAO);
			glBindVertexArray(m_VAO);
				glEnableVertexAttribArray(POSITION_LOCATION);
				glEnableVertexAttribArray(NORMAL_LOCATION);
				glEnableVertexAttribArray(TEXCOORDS_LOCATION);
				glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
					glVertexAttribPointer(	POSITION_LOCATION,
											cone.getPositionNumComponents(),
											cone.getDataType(),
											GL_FALSE,
											cone.getVertexByteSize(),
											cone.getPositionOffset()
					);
					glVertexAttribPointer(	NORMAL_LOCATION,
											cone.getNormalNumComponents(),
											cone.getDataType(),
											GL_FALSE,
											cone.getVertexByteSize(),
											cone.getNormalOffset()
					);
					glVertexAttribPointer(	TEXCOORDS_LOCATION,
											cone.getTexCoordsNumComponents(),
											cone.getDataType(),
											GL_FALSE,
											cone.getVertexByteSize(),
											cone.getTexCoordsOffset()
					);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
}

GLShapeInstance::GLShapeInstance (const imac2gl3::Cylinder & cylinder){
	
			glGenBuffers(1, &m_VBO);
			m_VertexCount = cylinder.getVertexCount();
			
			glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
				glBufferData(GL_ARRAY_BUFFER, cylinder.getByteSize(), cylinder.getDataPointer(), GL_STATIC_DRAW); 
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			
			glGenVertexArrays(1, &m_VAO);
			glBindVertexArray(m_VAO);
				glEnableVertexAttribArray(POSITION_LOCATION);
				glEnableVertexAttribArray(NORMAL_LOCATION);
				glEnableVertexAttribArray(TEXCOORDS_LOCATION);
				glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
					glVertexAttribPointer(	POSITION_LOCATION,
											cylinder.getPositionNumComponents(),
											cylinder.getDataType(),
											GL_FALSE,
											cylinder.getVertexByteSize(),
											cylinder.getPositionOffset()
					);
					glVertexAttribPointer(	NORMAL_LOCATION,
											cylinder.getNormalNumComponents(),
											cylinder.getDataType(),
											GL_FALSE,
											cylinder.getVertexByteSize(),
											cylinder.getNormalOffset()
					);
					glVertexAttribPointer(	TEXCOORDS_LOCATION,
											cylinder.getTexCoordsNumComponents(),
											cylinder.getDataType(),
											GL_FALSE,
											cylinder.getVertexByteSize(),
											cylinder.getTexCoordsOffset()
					);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
}

GLShapeInstance::~GLShapeInstance(){
	glDeleteBuffers(1, &m_VBO);
	glDeleteVertexArrays(1, &m_VAO);
}

void GLShapeInstance::draw() const{
	glBindVertexArray(m_VAO);
		glDrawArrays(GL_TRIANGLES, 0, m_VertexCount);
	glBindVertexArray(0);
}
